package demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.data.jpa.domain.AbstractPersistable;

/**
 * The persistent class for the city database table.
 * 
 */
@Entity
public class City extends AbstractPersistable<Integer> {
	private static final long serialVersionUID = 1L;
	
	@Override
	public String toString() {
		return "Anime [Anime_ID=" + Anime_ID + ", Name=" + Name + ", Text="
				+ Text + ", Episoden=" + Episoden + ", Status="
				+ Status + ", Typ_ID=" + Typ_ID + ", Erscheinungsdatum=" + Erscheinungsdatum + "]";
	}

	@Id
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	// Cannot be used with String type!
	// The default is assigned generator which means the value of identifier has to be set by the application.
	@Column(name="Anime_ID")
	private int Anime_ID;
	@Column(name="Name")
	private String Name;
	@Column(name="Text")
	private String Text;
	@Column(name="Episoden")
	private int Episoden;
	@Column(name="Status")
	private int Status;
	@Column(name="Typ_ID")
	private String Typ_ID;
	@Column(name="Erscheinungsdatum")
	private String Erscheinungsdatum;

	public City() {
	}

	public int getAnime_ID() {
		return Anime_ID;
	}

	public void setAnime_ID(int anime_ID) {
		Anime_ID = anime_ID;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getText() {
		return Text;
	}

	public void setText(String text) {
		Text = text;
	}

	public int getEpisoden() {
		return Episoden;
	}

	public void setEpisoden(int episoden) {
		Episoden = episoden;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getTyp_ID() {
		return Typ_ID;
	}

	public void setTyp_ID(String typ_ID) {
		Typ_ID = typ_ID;
	}

	public String getErscheinungsdatum() {
		return Erscheinungsdatum;
	}

	public void setErscheinungsdatum(String erscheinungsdatum) {
		Erscheinungsdatum = erscheinungsdatum;
	}
}