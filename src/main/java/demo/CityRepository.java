package demo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import java.lang.String;

import demo.City;

public interface CityRepository extends CrudRepository<City, Integer>
{
    List<City> findAll();    
    
    @Override
    public City findOne(Integer arg0);
    
    List<City> findByName(String name);
    
    @Override
    public long count();
    
    
    @Override
    public void delete(Integer arg0);
    
    @Override
    public boolean exists(Integer arg0);
    
}
